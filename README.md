# Project: Spring Multi-tenant CO2

Author: Nikos Athanasakis

### Usage

- Run with 'gradlew bootRun'.
- With swagger ui we cannot currently use header parameters. So it works only with the default 'master-tenant' schema.
- Only two tenant schemas have been currently configured: city1, city2 (can add more in
  src/main/resources/application.properties).
- The schemas at the start have no data (i.e. records of SensorReading).
- curl examples:

```
// Get all SensorReading for city1
curl -X GET "http://localhost:8080/v1/sensorReadings" -H "X-cityId: city1" -H "accept: application/json"
// Create a SensorReading for city1 for district1
curl -X POST "http://localhost:8080/v1/sensorReadings" -H "X-cityId: city1" -H "accept: */*" -H "Content-Type: application/json" -d "{\"city\":\"string\",\"co2Level\":0,\"district\":\"district1\",\"measurementTimestamp\":\"2021-01-25T10:24:55.814Z\",\"sensorReadingId\":0}"
// Get SensorReading with id '1' for city1
curl -X GET "http://localhost:8080/v1/sensorReadings/1" -H "X-cityId: city1" -H "accept: */*"
// Get all SensorReading for city1 for district1
curl -X GET "http://localhost:8080/v1/sensorReadings/search/findByDistrict?district=district1" -H "X-cityId: city1" -H "accept: */*"
```

A few more executable examples can be found here: http_request/examples.http

### Documentation / Inspection

- http://localhost:8080/swagger-ui/#/SensorReading%20Entity
- http://localhost:8080/h2-console (for jdbcUrl see src/main/resources/application.properties)

### Notes

- I've implemented multi-tenancy by way of having one schema per tenant/city. Every datasource has a dataSourceId. The
  api caller should provide a X-cityId header value matching dataSourceId. I haven't added any actual authentication
  handling (candidates e.g. JWT, OAuth).
- I've used the schema 'master-tenant' as a prototype for the other tenant schemas. It doesn't belong to any actual
  tenant/city.
- Spring exports ddl from schema 'master-tenant' at sql/export_scripts/create.sql.
- Spring updates schema 'master-tenant' based on the entities (when spring.jpa.hibernate.ddl-auto=update).
- Had no time to write tests for this quick project (although I consider them a must when on the job). 

