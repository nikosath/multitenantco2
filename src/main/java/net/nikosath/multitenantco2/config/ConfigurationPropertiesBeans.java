package net.nikosath.multitenantco2.config;

import net.nikosath.multitenantco2.config.multitenant.DataSourceProperties;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ConfigurationPropertiesBeans {

    public static final String MASTER_TENANT_DATA_SOURCE_PROPERTIES = "masterTenantDataSourceProperties";
    public static final String TENANT_DATA_SOURCE_PROPERTIES_LIST = "tenantDataSourcePropertiesList";

    @Bean
    @ConfigurationProperties(prefix = "spring.flyway")
    public FlywayProperties flywayProperties() {
        return new FlywayProperties();
    }

    @Bean(name = MASTER_TENANT_DATA_SOURCE_PROPERTIES)
    @ConfigurationProperties(prefix = "master-tenant.datasourceproperties")
    public DataSourceProperties masterTenantDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(TENANT_DATA_SOURCE_PROPERTIES_LIST)
    @ConfigurationProperties(prefix = "tenant.datasourceproperties")
    public List<DataSourceProperties> tenantDataSourcePropertiesList() {
        return new ArrayList<>();
    }

}
