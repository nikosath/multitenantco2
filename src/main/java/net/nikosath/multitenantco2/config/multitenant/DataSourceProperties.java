package net.nikosath.multitenantco2.config.multitenant;

import lombok.Data;

@Data
public class DataSourceProperties {
    private String datasourceId;
    private String jdbcUrl;
    private String driverClassName;
    private String username;
    private String password;
}
