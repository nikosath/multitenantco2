package net.nikosath.multitenantco2.config.multitenant.interceptor;

import net.nikosath.multitenantco2.config.multitenant.routing.ThreadLocalTenantContext;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

@Component
public class TenantHeaderInterceptor implements WebRequestInterceptor {

    public static final String TENANT_HEADER = "X-cityId";

    @Override
    public void preHandle(WebRequest request) throws Exception {
        ThreadLocalTenantContext.setTenantId(request.getHeader(TENANT_HEADER));
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {
        // No need to do something here
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {
        ThreadLocalTenantContext.removeTenantId();
    }
}
