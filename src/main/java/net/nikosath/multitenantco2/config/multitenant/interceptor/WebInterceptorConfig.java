package net.nikosath.multitenantco2.config.multitenant.interceptor;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;

@Configuration
@RequiredArgsConstructor
public class WebInterceptorConfig {

    private final TenantHeaderInterceptor tenantHeaderInterceptor;

    @Bean
    public MappedInterceptor registerTenantHeaderInterceptor() {
        return new MappedInterceptor(null, tenantHeaderInterceptor);
    }

}
