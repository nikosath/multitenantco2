package net.nikosath.multitenantco2.config.multitenant.routing;

import lombok.experimental.UtilityClass;

import java.util.Optional;

@UtilityClass
public class ThreadLocalTenantContext {

    private static final ThreadLocal<String> threadLocalTenantId = new ThreadLocal<>();

    public static Optional<String> getTenantId() {
        return Optional.ofNullable(threadLocalTenantId.get());
    }

    public static void setTenantId(String tenantId) {
        threadLocalTenantId.set(tenantId);
    }

    public static void removeTenantId() {
        threadLocalTenantId.remove();
    }
}
