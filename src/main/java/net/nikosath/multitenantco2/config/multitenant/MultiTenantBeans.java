package net.nikosath.multitenantco2.config.multitenant;

import net.nikosath.multitenantco2.config.multitenant.resources.MasterTenantResources;
import net.nikosath.multitenantco2.config.multitenant.resources.TenantResources;
import net.nikosath.multitenantco2.config.multitenant.routing.TenantRoutingDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.List;

import static net.nikosath.multitenantco2.config.ConfigurationPropertiesBeans.MASTER_TENANT_DATA_SOURCE_PROPERTIES;
import static net.nikosath.multitenantco2.config.ConfigurationPropertiesBeans.TENANT_DATA_SOURCE_PROPERTIES_LIST;

@Configuration
public class MultiTenantBeans {

    private final DataSourceProperties masterTenantDataSourceProperties;
    private final List<DataSourceProperties> tenantDataSourcePropertiesList;

    public MultiTenantBeans(@Qualifier(MASTER_TENANT_DATA_SOURCE_PROPERTIES)
                                    DataSourceProperties masterTenantDataSourceProperties,
                            @Qualifier(TENANT_DATA_SOURCE_PROPERTIES_LIST)
                                    List<DataSourceProperties> tenantDataSourcePropertiesList) {

        this.masterTenantDataSourceProperties = masterTenantDataSourceProperties;
        this.tenantDataSourcePropertiesList = tenantDataSourcePropertiesList;
    }

    @Bean
    public MasterTenantResources masterTenantResources() {
        return new MasterTenantResources(masterTenantDataSourceProperties);
    }

    @Bean
    public TenantResources tenantResources() {
        return new TenantResources(tenantDataSourcePropertiesList);
    }

    @Bean
    public DataSource tenantRoutingDatasource() {
        return new TenantRoutingDataSource(masterTenantResources(), tenantResources());
    }

}
