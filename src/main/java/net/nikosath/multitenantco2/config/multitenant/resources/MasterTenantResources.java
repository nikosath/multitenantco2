package net.nikosath.multitenantco2.config.multitenant.resources;

import lombok.Getter;
import net.nikosath.multitenantco2.config.multitenant.DataSourceProperties;
import net.nikosath.multitenantco2.util.DatasourceUtils;

import javax.sql.DataSource;

public class MasterTenantResources {

    @Getter
    private final DataSourceProperties dataSourceProperties;
    @Getter
    private final DataSource datasource;

    public MasterTenantResources(DataSourceProperties dataSourceProperties) {
        this.dataSourceProperties = dataSourceProperties;
        this.datasource = DatasourceUtils.createDatasourceFrom(dataSourceProperties);
    }

    public String getDatasourceId() {
        return dataSourceProperties.getDatasourceId();
    }
}
