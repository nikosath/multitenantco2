package net.nikosath.multitenantco2.config.multitenant.resources;

import lombok.Getter;
import net.nikosath.multitenantco2.config.multitenant.DataSourceProperties;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.nikosath.multitenantco2.util.DatasourceUtils.createDatasourceFrom;

public class TenantResources {

    @Getter
    private final Map<String, DataSource> dataSourceMap;

    public TenantResources(List<DataSourceProperties> dataSourcePropertiesList) {
        this.dataSourceMap = createTenantDataSourceMap(dataSourcePropertiesList);
    }

    private Map<String, DataSource> createTenantDataSourceMap(List<DataSourceProperties> dataSourcePropertiesList) {
        HashMap<String, DataSource> map = new HashMap<>();
        dataSourcePropertiesList.forEach(
                dp -> map.put(dp.getDatasourceId(), createDatasourceFrom(dp)));
        return map;
    }

}
