package net.nikosath.multitenantco2.config.multitenant;

import lombok.RequiredArgsConstructor;
import net.nikosath.multitenantco2.config.multitenant.routing.TenantRoutingDataSource;
import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty(name = "initialize-tenant-schemas", havingValue = "true")
public class TenantSchemasInitializer {

    private final TenantRoutingDataSource tenantRoutingDataSource;
    private final FlywayProperties flywayProperties;

    @PostConstruct
    public void initializeTenantSchemas() {
        tenantRoutingDataSource.getResolvedDataSources().values().forEach(
                dataSource -> {
                    String[] scriptLocations = flywayProperties.getLocations().toArray(String[]::new);

                    Flyway flyway = Flyway.configure()
                            .dataSource(dataSource)
                            .locations(scriptLocations)
                            .load();

                    flyway.migrate();
                }
        );
    }

}
