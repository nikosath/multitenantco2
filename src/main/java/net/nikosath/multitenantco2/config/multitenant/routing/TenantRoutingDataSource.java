package net.nikosath.multitenantco2.config.multitenant.routing;

import net.nikosath.multitenantco2.config.multitenant.resources.MasterTenantResources;
import net.nikosath.multitenantco2.config.multitenant.resources.TenantResources;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.HashMap;
import java.util.Map;

public class TenantRoutingDataSource extends AbstractRoutingDataSource {

    private final MasterTenantResources masterTenantResources;

    public TenantRoutingDataSource(MasterTenantResources masterTenantResources, TenantResources tenantResources) {
        this.masterTenantResources = masterTenantResources;
        this.setTargetDataSources(createTenantDataSourceMap(tenantResources));
    }

    private Map<Object, Object> createTenantDataSourceMap(TenantResources tenantResources) {

        Map<Object, Object> tenantDataSourceMap = new HashMap<>(tenantResources.getDataSourceMap());
        tenantDataSourceMap.put(masterTenantResources.getDatasourceId(), masterTenantResources.getDatasource());
        return tenantDataSourceMap;
    }

    @Override
    protected String determineCurrentLookupKey() {
        return ThreadLocalTenantContext.getTenantId()
                .orElse(masterTenantResources.getDatasourceId());
    }

}
