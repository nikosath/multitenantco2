package net.nikosath.multitenantco2.repository;

import net.nikosath.multitenantco2.entitiy.SensorReading;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface SensorReadingRepository extends PagingAndSortingRepository<SensorReading, Long> {
    Iterable<SensorReading> findByDistrict(@Param("district") String district);
}
