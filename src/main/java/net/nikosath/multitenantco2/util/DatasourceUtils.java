package net.nikosath.multitenantco2.util;

import lombok.experimental.UtilityClass;
import net.nikosath.multitenantco2.config.multitenant.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;

@UtilityClass
public class DatasourceUtils {

    public static DataSource createDatasourceFrom(DataSourceProperties dp) {
        return DataSourceBuilder.create()
                .url(dp.getJdbcUrl())
                .driverClassName(dp.getDriverClassName())
                .username(dp.getUsername())
                .password(dp.getPassword())
                .build();
    }

}
