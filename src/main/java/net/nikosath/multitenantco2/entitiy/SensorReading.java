package net.nikosath.multitenantco2.entitiy;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
// TODO: validate
public class SensorReading {
    // TODO: switch to GenerationType.SEQUENCE?
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long sensorReadingId;
    LocalDateTime measurementTimestamp;
    Double co2Level;
    // TODO: remove 'city'?
    String city;
    String district;
}
